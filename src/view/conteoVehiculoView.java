package view;

import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

public class conteoVehiculoView extends javax.swing.JFrame {

    public conteoVehiculoView() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelAbajo = new javax.swing.JPanel();
        lbAP = new javax.swing.JLabel();
        lbT = new javax.swing.JLabel();
        lbC = new javax.swing.JLabel();
        lbB = new javax.swing.JLabel();
        lbVM = new javax.swing.JLabel();
        lbTotal = new javax.swing.JLabel();
        btoSalir = new javax.swing.JButton();
        btoAP = new javax.swing.JButton();
        btoTaxis = new javax.swing.JButton();
        btoCombis = new javax.swing.JButton();
        btoBuses = new javax.swing.JButton();
        btoVM = new javax.swing.JButton();
        btoReinicio = new javax.swing.JButton();
        fieldTotal = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Conteo de Vehiculos");
        setMinimumSize(new java.awt.Dimension(500, 300));
        setPreferredSize(new java.awt.Dimension(500, 300));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panelAbajo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbAP.setFont(new java.awt.Font("Decker", 1, 12)); // NOI18N
        lbAP.setText("Automoviles Particulares:");
        panelAbajo.add(lbAP, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 50, -1, -1));

        lbT.setFont(new java.awt.Font("Decker", 1, 12)); // NOI18N
        lbT.setText("Taxis:");
        panelAbajo.add(lbT, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, -1));

        lbC.setFont(new java.awt.Font("Decker", 1, 12)); // NOI18N
        lbC.setText("Combis:");
        panelAbajo.add(lbC, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, -1, -1));

        lbB.setFont(new java.awt.Font("Decker", 1, 12)); // NOI18N
        lbB.setText("Buses:");
        panelAbajo.add(lbB, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 170, -1, -1));

        lbVM.setFont(new java.awt.Font("Decker", 1, 12)); // NOI18N
        lbVM.setText("Vehículos Menores:");
        panelAbajo.add(lbVM, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, -1, -1));

        lbTotal.setFont(new java.awt.Font("Decker", 1, 12)); // NOI18N
        lbTotal.setText("Total:");
        panelAbajo.add(lbTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 220, -1, -1));

        btoSalir.setFont(new java.awt.Font("Decker", 1, 18)); // NOI18N
        btoSalir.setText("Salir");
        btoSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btoSalirActionPerformed(evt);
            }
        });
        panelAbajo.add(btoSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 110, 110, 50));

        btoAP.setText("0");
        btoAP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btoAPActionPerformed(evt);
            }
        });
        panelAbajo.add(btoAP, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 40, 60, 30));

        btoTaxis.setText("0");
        btoTaxis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btoTaxisActionPerformed(evt);
            }
        });
        panelAbajo.add(btoTaxis, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 80, 60, 30));

        btoCombis.setText("0");
        btoCombis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btoCombisActionPerformed(evt);
            }
        });
        panelAbajo.add(btoCombis, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 120, 60, 30));

        btoBuses.setText("0");
        btoBuses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btoBusesActionPerformed(evt);
            }
        });
        panelAbajo.add(btoBuses, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 160, 60, 30));

        btoVM.setText("0");
        btoVM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btoVMActionPerformed(evt);
            }
        });
        panelAbajo.add(btoVM, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 200, 60, 30));

        btoReinicio.setFont(new java.awt.Font("Decker", 1, 14)); // NOI18N
        btoReinicio.setText("<html><p>Reiniciar<br>Conteo</p></html>");
        btoReinicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btoReinicioActionPerformed(evt);
            }
        });
        panelAbajo.add(btoReinicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 40, 110, 60));
        panelAbajo.add(fieldTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 220, 80, -1));

        getContentPane().add(panelAbajo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 480, 300));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btoAPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btoAPActionPerformed
        actionPerfomance(evt);
    }//GEN-LAST:event_btoAPActionPerformed

    private void btoTaxisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btoTaxisActionPerformed
        actionPerfomance(evt);
    }//GEN-LAST:event_btoTaxisActionPerformed

    private void btoCombisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btoCombisActionPerformed
        actionPerfomance(evt);
    }//GEN-LAST:event_btoCombisActionPerformed

    private void btoBusesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btoBusesActionPerformed
        actionPerfomance(evt);
    }//GEN-LAST:event_btoBusesActionPerformed

    private void btoVMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btoVMActionPerformed
        actionPerfomance(evt);
    }//GEN-LAST:event_btoVMActionPerformed

    private void btoReinicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btoReinicioActionPerformed
        reiniciarContador();
    }//GEN-LAST:event_btoReinicioActionPerformed

    private void btoSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btoSalirActionPerformed
        try {
            int mostrar = JOptionPane.showConfirmDialog(null, "¿Desea cerrar el sistema?", "Salir", JOptionPane.YES_NO_OPTION);
            if (mostrar == 0) {
                System.exit(0);
            }
        } catch (Exception e) {

        }
    }//GEN-LAST:event_btoSalirActionPerformed
    //Declaración de Contadores para los Vehículos
    private int contadorAP = 0;
    private int contadorTaxis = 0;
    private int contadorCombis = 0;
    private int contadorBuses = 0;
    private int contadorVM = 0;
    
    //Declaración de las tarifas de los Vehiculos
    private double tarifaAP = 3.5;
    private double tarifaTaxis = 3.0;
    private double tarifaCombis = 4.0;
    private double tarifaBuses = 6.0;
    private double tarifaVM = 2.0;
   
    //Declaracion de variables para los totales
    private double cAP = 0;
    private double cTaxis = 0;
    private double cCombis = 0;
    private double cBuses = 0;
    private double cVM = 0;
    
    // Sumatoria de las tarifas
    private double total = 0;

    private void actionPerfomance(ActionEvent evt) {
        //Contador para Autos Particulares
        if (evt.getSource() == this.btoAP) {
            contadorAP++;
            String countAP = String.valueOf(contadorAP);
            btoAP.setText(countAP);
             cAP = contadorAP * tarifaAP;
        } else//Contador para Taxis
        if (evt.getSource() == this.btoTaxis) {
            contadorTaxis++;
            String countT = String.valueOf(contadorTaxis);
            btoTaxis.setText(countT);
            cTaxis = contadorTaxis * tarifaTaxis;
        }else//Contador para Combis
        if (evt.getSource() == this.btoCombis) {
            contadorCombis++;
            String countC = String.valueOf(contadorCombis);
            btoCombis.setText(countC);
            cCombis =  contadorCombis * tarifaCombis;
        }
        else//Contador para Buses
        if (evt.getSource() == this.btoBuses) {
            contadorBuses++;
            String countB = String.valueOf(contadorBuses);
            btoBuses.setText(countB);
            cBuses = contadorBuses * tarifaBuses;
        }
            else//Contador para Vehiculos Menores
        if (evt.getSource() == this.btoVM) {
           contadorVM++;
            String countVM = String.valueOf(contadorVM);
            btoVM.setText(countVM);
            cVM = contadorVM * tarifaVM;
        }
        
        total = cAP+cBuses+cCombis+cTaxis+cVM;
        String totalTarifa = String.valueOf(total);
        fieldTotal.setText(totalTarifa);
        
    }
private void reiniciarContador(){
    contadorAP = 0;
    contadorTaxis = 0;
    contadorCombis = 0;
    contadorBuses = 0;
    contadorVM = 0;
    total = 0;
    cAP = 0;
    cBuses = 0;
    cCombis = 0;
    cVM = 0;
    cTaxis = 0;
    btoAP.setText("0");
    btoTaxis.setText("0");
    btoCombis.setText("0");
    btoBuses.setText("0");
    btoVM.setText("0");
    fieldTotal.setText("");
    
}
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new conteoVehiculoView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btoAP;
    private javax.swing.JButton btoBuses;
    private javax.swing.JButton btoCombis;
    private javax.swing.JButton btoReinicio;
    private javax.swing.JButton btoSalir;
    private javax.swing.JButton btoTaxis;
    private javax.swing.JButton btoVM;
    private javax.swing.JTextField fieldTotal;
    private javax.swing.JLabel lbAP;
    private javax.swing.JLabel lbB;
    private javax.swing.JLabel lbC;
    private javax.swing.JLabel lbT;
    private javax.swing.JLabel lbTotal;
    private javax.swing.JLabel lbVM;
    private javax.swing.JPanel panelAbajo;
    // End of variables declaration//GEN-END:variables
}
